FROM golang:1.8.3-alpine
RUN apk add --update --no-cache git make autoconf curl libtool automake gcc alpine-sdk unzip && \
        git clone https://github.com/google/protobuf.git && \
        cd protobuf && \
        git submodule update --init --recursive && \
        ./autogen.sh && \
        ./configure && \
        make && \
        make check && \
        make install && \
        ldconfig / && \
        make clean && \
        cd .. && \
        rm -r protobuf && \
        go get google.golang.org/grpc && \
        go get github.com/golang/protobuf/protoc-gen-go && \
        go get -u github.com/gengo/grpc-gateway/protoc-gen-grpc-gateway && \
        go get -u github.com/golang/protobuf/protoc-gen-go
