CONTAINER_NAME=grpc-go
LANG=go

build:
	@docker build -t $(CONTAINER_NAME) .

clean:
	@docker run --rm -it -v $(CURDIR):$(CURDIR) -w $(CURDIR) alpine \
	rm -rf ./output/*

$(LANG):
	@docker run --rm -it -v $(CURDIR):$(CURDIR) -w $(CURDIR) \
	$(CONTAINER_NAME) /bin/bash -c 'protoc --proto_path=./example/ --$@_out=plugins=grpc:./output service.proto'

.PHONY: example build clean

.DEFAULT_GOAL :=$(LANG)
